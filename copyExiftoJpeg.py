#!/usr/bin/env python3
import datetime, pytz
import subprocess, json, os, glob
import pprint

pp = pprint.PrettyPrinter(indent=2)


class ExifCopy():
    def __init__(self, folder='converted/', sourcefolder='./'):
        self.folder = folder
        self.sourcefolder = sourcefolder
        self.sourcesdict = self.getSourcelist()
        self.filelist = self.getFilelist()
        self.timeDiff = self.checkTimeDiff()
        self.run()

    def run(self):
        print(type(self.timeDiff))
        for f in self.filelist:
            self.copyExif(f)

    def createTimeValues(self, timeStr):
        imgTime = datetime.datetime.strptime(timeStr, '%Y:%m:%d %H:%M:%S.%f%z').astimezone(pytz.utc)
        newTime = (imgTime + self.timeDiff).astimezone(pytz.timezone('Europe/Amsterdam'))
        date = newTime.strftime('%Y:%m:%d')
        time = newTime.strftime('%H:%M:%S')
        microseconds = newTime.strftime('%f')[:2]
        timezone = f"{newTime.strftime('%z')[:3]}:{newTime.strftime('%z')[-2:]}"

        times = {}
        times['-DateTime'] = f'{date} {time}'
        times['-DateTimeOriginal'] = times['-DateTime']
        times['-CreateDate'] = times['-DateTime']
        times['-TimeStamp'] = times['-DateTime']
        times['-SubSecDateTimeOriginal'] = f'{times["-DateTime"]}.{microseconds}{timezone}'
        times['-SubSecCreateDate'] = times['-SubSecDateTimeOriginal']
        times['-SubSecModifyDate'] = times['-SubSecDateTimeOriginal']
        times['-TimeZone'] = timezone
        return times

    def adjustTime(self, file):
        output = subprocess.check_output(['exiftool', '-j', '-*Date*', file])
        data = json.loads(output.decode('utf-8'))

        dict = self.createTimeValues(data[0]['SubSecDateTimeOriginal'])
        cmd = ['exiftool', '-overwrite_original']
        for tag, val in dict.items():
            cmd.append(f'{tag}={val}')
        cmd.append(file)
        subprocess.call(cmd)

    def getFilelist(self):
        return glob.glob(f'{self.folder}*.jpg')

    def getBaseName(self, file):
        return file.rsplit("/", 1)[-1].split("__")[0].split("-")[0].split(".")[0]

    def copyExif(self, file):
        basename = self.getBaseName(file)
        if basename in self.sourcesdict:
            print(f"INF {file}: replace with exif from {self.sourcesdict[basename]}")
            cmd = ['exiftool', '-TagsFromFile', self.sourcesdict[basename], '--SubSecModifyDate', '-overwrite_original',
                   '--ModifyDate', '--AspectRatio', '--Orientation', '--MiniatureFilterOrientation', file]
            subprocess.call(cmd)
            if self.timeDiff is not None:
                self.adjustTime(file)
        else:
            print(f"ERR {file}: no sourcefile found")

    def getSourcelist(self):
        result = {}
        filelist = glob.glob(f'{self.sourcefolder}*.CR3')
        for file in filelist:
            result[self.getBaseName(file)] = file
        return result

    def checkTimeDiff(self):
        timeDiff = None
        days = 0

        if os.path.isfile(f'{self.sourcefolder}timediff.txt'):
            try:
                with open("timediff.txt", 'r') as f:
                    timeDiff = f.read().strip()
                    timeDiff = timeDiff.split(',')
                    if 'days' in timeDiff[0]:
                        days = int(timeDiff[0].split(" ")[0])
                    hours, minutes, seconds = timeDiff[-1].strip().split(":")
                    timeDiff = datetime.timedelta(days=days, hours=int(hours), minutes=int(minutes),
                                                  seconds=int(seconds))
            except IOError:
                print("Err Timediff file is not readable")

        return timeDiff


if __name__ == "__main__":
    ExifCopy()


def run(args=None):
    exif = ExifCopy()
    return 0
