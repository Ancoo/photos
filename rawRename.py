#!/usr/bin/env python3

import subprocess, json, os
import pprint

pp = pprint.PrettyPrinter(indent=2)

renamed = 0
notRenamed = 0


def getExifData(folder):
    # Get all the exif data from the Canon CR3 files
    cmd = "exiftool -j *.CR3"
    output = subprocess.check_output(cmd, shell=True)
    return json.loads(output.decode('utf-8'))


def getNewName(item):
    global notRenamed
    if "__F_" in item['FileName']:
        notRenamed += 1
        return False  # already done, no need to do it again
    base, extension = item['FileName'].rsplit('.', 1)
    shutterspeed = str(item['ShutterSpeed']).replace("/", ".")
    focallength = item['FocalLength'].replace(" ", "")
    aperture = (item['Aperture'])
    iso = item['ISO']
    return f'{base}__F_{aperture}__{shutterspeed}s_ISO_{iso}__{focallength}.{extension}'


def rename(item):
    global renamed
    os.rename(item['FileName'], item['NewName'])
    renamed += 1
    if os.path.exists(f"{item['FileName']}.pp3"):
        os.rename(f"{item['FileName']}.pp3", f"{item['NewName']}.pp3")
        print(f"pp3 file exists for {item['FileName']}")


def run(args, folder="."):
    data = getExifData(folder)
    for item in data:
        name = getNewName(item)
        if name:
            item['NewName'] = name
            rename(item)

    print(f'{renamed} image file(s) renamed')
    print(f'{notRenamed} image file(s) were already renamed')
    return 0


if __name__ == "__main__":
    run(None)
