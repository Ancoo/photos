#!/usr/bin/env python3

import subprocess, glob
import pprint
pp = pprint.PrettyPrinter(indent=2)


def run(args):
    cmd = ["exiftool", "-geotag"]
    result = 0
    gpsfiles = []
    if args.synctime is not None:
        cmd.append(f"-geosync={args.synctime[0]}")
    if args.sourceFile is not None:
        for f in args.sourceFile:
            gpsfiles.append(f.name)
    else:
        gpsfiles = glob.glob('*.gpx')

    for f in gpsfiles:
        # print (cmd + [f, 'converted/'])
        result = subprocess.call(cmd + [f, 'converted/'])
    return result

