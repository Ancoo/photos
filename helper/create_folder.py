#!/usr/bin/env python3

import os


def create_folder(folder):
    if not os.path.exists(folder):
        os.mkdir(folder)
        print(f"Dreated directiory: {folder} ")
    else:
        if not os.path.isdir(folder):
            print(f"ERR: {folder} exists but is not a folder. Exit")
            exit()
        # else:
        #     print("Destination folder found")