#!/usr/bin/env python3
import argparse, pathlib


def addCleanupArgs(parser):
    parser.description = 'Delete photos in other format to keep FS cleaner'
    parser.add_argument(dest="directory", help="Dir to run check files", nargs="?")
    parser.add_argument('-d', '--dry-run', action='store_true', help="Show results, don't execute actions")
    parser.add_argument('-v', '--verbose', action='count', help="Show more output")


def timeadjustArgs(timeadjust):
    timeadjust.description = 'fix the timestamp if time in the camera was wrong'
    timeadjust.add_argument(
        "-t", "--time",
        type=str,
        nargs=1,
        help="give correct time (localized) for SOURCEFILE in HH:MM:SS, default is current time of picture"
    )
    timeadjust.add_argument(
        "-d", "--date",
        type=str,
        nargs=1,
        help="give correct time for SOURCEFILE in YYMMDD, default is current date of picture"
    )
    opttimeadjust = timeadjust.add_mutually_exclusive_group(required=True)
    opttimeadjust.add_argument(
        "-f", "--sourceFile",
        type=argparse.FileType('r'),
        nargs=1,
        default=None,
        help="sourcefile to use as reference time for timeadjust"
    )

    opttimeadjust.add_argument(
        "-r", "--reset",
        action="store_true",
        help="reset the difftime file"
    )


def gpsArg(gps):
    gps.add_argument(
        "-f", "--sourceFile",
        type=argparse.FileType('r'),
        nargs='+',
        default=None,
        help="sourcefile to use as reference time for gps. default is to run for all .gpx files"
    )
    gps.add_argument(
        "-s", "--sync",
        type=str,
        nargs=1,
        default=None,
        dest='synctime',
        help="Sync time between gps and img timestamps. -s +1:20 adds 1 minute and 20 seconds to the image time"
    )


def addBackupArgs(backup):
    backup.description = "Upload a folder to cloud"
    backup.add_argument(
        "folder",
        type=pathlib.Path,
        nargs=1,
        help="Folder to backup "
    )


def addSettierArgs(settier):
    settier.description = "change the tier of folder on S3"

    settier.add_argument('tier', nargs=1, type=str, choices=['cold', 'glacier', 'restore'])
    settier.add_argument(
        "folder",
        type=pathlib.Path,
        nargs=1,
        help="Folder to change settier of"
    )


def rawtherapee(rawtherapee):
    rawtherapee.add_argument(
        "args",
        nargs="*",
        help="args? "
    )


def blend(blend):
    blend.add_argument(
        "n",
        nargs=1,
        type=int,
        help="Amount of images to blend"
    )
    blend.add_argument(
        "-d", "--dest",
        nargs='?',
        type=str,
        default="blend/",
        help="Folder to save images in"
    )
    blend.add_argument(
        "-p", "--prefix",
        nargs="?",
        type=str,
        default="img_",
        help="Prefix before the numbering (default: img_)"
    )

    blend.add_argument(
        "-g", "--glob",
        nargs="+",
        type=str,
        help="searchstring for the files, make sure that you only find the desired files with it (so only jpg)"
    )
    blend.add_argument("-a", "--all", action="store_true",
                       help='Also produce blends of less than N images at begin and end of sequence. Default = True')


def export(export):
    export.add_argument(
        "-d", "--dest",
        nargs='?',
        type=str,
        default="export/",
        help="Folder to save images in. default is export/ "
    )

    export.add_argument(
        "-i", "--input",
        nargs='*',
        type=str,
        default="./",
        help="Folders or files that are the source. If it is a folder, will look for all .pp3 files in that. "
             "Default: current folder"
    )

    export.add_argument(
        "-s", "--source",
        nargs='?',
        type=str,
        default="../",
        help="Folders location of CR3 files, default is parent folder"
    )

    export.add_argument(
        "-t", "--threads",
        nargs='?',
        type=int,
        default=1,
        help="Number of threads to use, default 1"
    )

    export.add_argument(
        "-p", "--prefix",
        nargs="?",
        type=str,
        default="",
        help="Prefix before the numbering (default: img_)"
    )

    export.add_argument("--overwrite", action="store_true",
                       help='Overwrite existing files')

    export.add_argument("-k", "--keep-pp3", action="store_true",
                       help='Save pp3 along create images')


