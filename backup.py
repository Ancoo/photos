#!/usr/bin/env python3
import os.path
import subprocess, json, datetime, glob
import pprint
import pytz
import zoneinfo
import settings
pp = pprint.PrettyPrinter(indent=2)

def trim(str):
    return str if str[-1] != "/" else trim(str[:-1])


def settier(args, folderTrimmed):
    print(args)
    storageclass = None
    if args.tier[0] == "restore":
        storageclass = "restore"
        cmd = f'rclone -i backend restore --include "{folderTrimmed}.tar.gz" scaleway:acg-photos/ -o priority=Standard'
    elif args.tier[0] == "cold" or args.tier[0] == "glacier":
        storageclass = "GLACIER"
        cmd = f"rclone settier {storageclass} {settings.s3server}:{settings.s3_backup_bucket}{folderTrimmed}.tar.gz"

    if storageclass:
        print(cmd)
        subprocess.run(cmd, shell=True, check=True)



def run(args):
    folder = args.folder[0]
    if not os.path.isdir(folder):
        print("ERR: Given foldername is not a folder, check the name")
        return 1

    folderTrimmed = trim(folder.name)
    folderName = folderTrimmed + "/"
    if args.maincommand == "settier":
        return settier(args, folderTrimmed)
    else:
        cmd = f"tar  --exclude='*.dng' -cz -O {folderName} | rclone rcat --progress --s3-chunk-size='50M' "
        cmd = f"{cmd}--s3-max-upload-parts=10000 --s3-acl=public-read --s3-storage-class=STANDARD"
        cmd = f"{cmd} {settings.s3server}:{settings.s3_backup_bucket}{folderTrimmed}.tar.gz"
        print(cmd)
        subprocess.run(cmd, shell=True, check=True)


