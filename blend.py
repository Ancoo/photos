#!/usr/bin/env python3

import glob
import os
from helper.create_folder import create_folder


def run(args, ):
    files = []

    if args.glob:
        for s in args.glob:
            files.extend(glob.glob(s))
    else:
        files.extend(glob.glob('*.jpg'))
        files.extend(glob.glob('*.JPG'))

    files.sort()

    n = args.n[0]

    start = 0
    offset = -((n-1)//2)
    end = len(files)-1+offset
    count = 0
    if args.all:
        start = -n+1
        end = len(files)

    create_folder(args.dest)

    for i in range(start, end):
        count += 1

        begin = 0 if i < 0 else i
        fList = " ".join(files[begin:i+n])
        outputName = f'{args.dest}/{args.prefix}{str(count).zfill(5)}.jpg'
        print(f'Creating number {count} : {fList}')
        os.system(f'magick {fList} -evaluate-sequence mean {outputName}')
#  magick anthony_1.jpg anthony_2.jpg -evaluate-sequence mean  anthony_ghosts.jpg


 #   print(files)