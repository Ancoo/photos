#!/usr/bin/env python3

import argparse, os
import arguments


def getfiles(arg):
    img = {'jpg': [], 'raw': [], 'dng': []}
    os.chdir(arg.directory)

    for item in os.listdir():
        if item.endswith('.jpg') or item.endswith('.JPG'):
            img['jpg'].append(item[:-3])
        elif item.endswith('.CR3'):
            img['raw'].append(item[:-3])

    if os.path.isdir(os.path.join(arg.directory, "dng")):
        os.chdir(os.path.join(arg.directory, "dng"))
        for item in os.listdir():
            if item.endswith('.dng'):
                img['dng'].append(item[:-3])

    os.chdir(arg.directory)

    if arg.verbose:
        print(f'Found {len(img["jpg"])} jpg file(s)')
        if arg.verbose > 1:
            print("list of jpg: ")
            print(img['jpg'])
        print(f'Found {len(img["raw"])} raw file(s)')
        if arg.verbose > 1:
            print("list of cr2: ")
            print(img['raw'])
        print(f'Found {len(img["dng"])} dng file(s)')
        if arg.verbose > 1:
            print("list of dng: ")
            print(img['dng'])
    return img


def find_to_delete(data, arg):
    img_to_del = {'jpg': set(), 'raw': set(), 'dng': set()}

    for img in data['dng']:
        if img not in data['jpg']:
            img_to_del['dng'].add(os.path.join("dng", img + "dng"))

    if not len(data['dng']):
        if arg.verbose:
            print("no dng files found, skip comparing with dng files")
    else:
        for img in data['jpg']:
            if img not in data['dng']:
                img_to_del['jpg'].add(img + "JPG")
        for img in data['raw']:
            if img not in data['dng']:
                img_to_del['raw'].add(img + "CR3")

    for img in data['raw']:
        if img not in data['jpg']:
            img_to_del['raw'].add(os.path.join(img + "CR3"))

    return img_to_del


def delete_img(arg, to_delete):
    os.chdir(arg.directory)
    if not os.path.exists("deleted"):
        os.mkdir("deleted")
    del_folder = os.path.join(arg.directory, "deleted")
    if not os.path.isdir(del_folder):
        raise Exception("Error: deleted exists but is not a valid folder")

    for key in to_delete:
        for item in to_delete[key]:
            os.rename(item, os.path.join(del_folder, os.path.basename(item)))
            if arg.verbose:
                print(f'moved {item} to {os.path.join(del_folder, os.path.basename(item))}')


def run(arg):
    print(arg)
    directory = arg.directory
    if directory is None:
        directory = os.getcwd()
        arg.directory = directory
    if not os.path.isdir(directory):
        raise Exception("Error: Directory is not a valid folder")
    if arg.verbose:
        print(f'Selected directory: {directory}')

    images = getfiles(arg)
    to_delete = find_to_delete(images, arg)
    if arg.verbose or arg.dry_run:
        print(f'Number of jpg file(s) to be removed: {len(to_delete["jpg"])}')
        if arg.dry_run or arg.verbose > 1:
            for item in to_delete['jpg']:
                print(item)
        print(f'Number of raw file(s) to be removed: {len(to_delete["raw"])}')
        if arg.dry_run or arg.verbose > 1:
            for item in to_delete['raw']:
                print(item)
        print(f'Number of dng file(s) to be removed: {len(to_delete["dng"])}')
        if arg.dry_run or arg.verbose > 1:
            for item in to_delete['dng']:
                print(item)

    if not arg.dry_run:
        delete_img(arg, to_delete)
    return 0


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    arguments.addCleanupArgs(parser)
    args = parser.parse_args()
    run(args)
