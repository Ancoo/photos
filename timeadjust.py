#!/usr/bin/env python3

import subprocess, json, datetime
import pprint
import pytz
import zoneinfo


pp = pprint.PrettyPrinter(indent=2)


class TimeAdjust():
    def __init__(self, sourcefile, date, time):
        self.sourcefile = sourcefile
        self.date = date
        self.time = time
        self.data = self.getExifData()
        self.timeDiff = self.calcDiff()
        print(self.timeDiff)
        write(self.timeDiff)

    def getExifData(self):
        # Get all the exif data from the Canon CR3 files
        cmd = ["exiftool", "-j", self.sourcefile]
        output = subprocess.check_output(cmd)

        return json.loads(output.decode('utf-8'))[0]

    def calcDiff(self):

        imgTime = datetime.datetime.strptime(self.data['SubSecDateTimeOriginal'], '%Y:%m:%d %H:%M:%S.%f%z')
        correctTime = imgTime

        if self.date:
            correctTime = correctTime.replace(year=int(self.date[0][:4]), month=int(self.date[0][4:6]),
                                              day=int(self.date[0][6:]))

        if self.time:
            time = self.time[0].split(":")
            correctTime = correctTime.replace(hour=int(time[0]), minute=int(time[1]), second=int(time[2]),
                                              tzinfo=zoneinfo.ZoneInfo('Europe/Amsterdam'))

        print(correctTime.isoformat())
        correctTime = correctTime.astimezone(pytz.utc)
        imgTime = imgTime.astimezone(pytz.utc)
        return correctTime - imgTime


def write(diff):
    with open("timediff.txt", "w") as f:
        # Writing data to a file
        f.write(f"{diff}\n")


def run(args):
    if args.reset:
        d = datetime.datetime.now()
        write(d - d)
    TimeAdjust(args.sourceFile[0].name, args.date, args.time)
