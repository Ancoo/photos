#!/usr/bin/env python3

import argparse
import pprint
import arguments

pp = pprint.PrettyPrinter(indent=2)


def init_argparse() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(
        # usage="%(prog)s [COMMAND] [FILE]...",
        description="Program to help organize / fix raw files"
    )

    subparser = parser.add_subparsers(dest='maincommand', required=True)
    rename = subparser.add_parser('rename', help='rename the .CR3 and PP3 files to show iso shutterspeed and aperture')
    exif = subparser.add_parser('exif', help='copy the .CR3 exif data to the exported jpg in the converted folder')

    gps = subparser.add_parser('gps', help="add gps locations to exported jpeg files from gps file")
    arguments.gpsArg(gps)

    cleanup = subparser.add_parser('cleanup',
                                   help='deletes corresponding CR3 file if JPEG file is deleted or the other way around')
    arguments.addCleanupArgs(cleanup)

    backup = subparser.add_parser('backup', help='Backup the folder to the cloud')
    arguments.addBackupArgs(backup)

    settier = subparser.add_parser('settier', help='change the tier of folder on S3')
    arguments.addSettierArgs(settier)

    timeadjust = subparser.add_parser('timeadjust',
                                      help="create timediff file to use with exif to correct creation date")
    arguments.timeadjustArgs(timeadjust)

    rawtherapee = subparser.add_parser('rawtherapee', help="create pp3 file for image")
    arguments.rawtherapee(rawtherapee)

    blend = subparser.add_parser('blend', help="Blend multiple jpg's to one image, to create long exposures")
    arguments.blend(blend)

    export = subparser.add_parser('export', help="Export to jpg from cr3 and pp3 files")
    arguments.export(export)

    parser.add_argument("-v", "--version", action="version",
                        version=f"{parser.prog} version 1.0.0"
                        )

    return parser


def main() -> None:
    parser = init_argparse()
    args = parser.parse_args()

    programs = {'cleanup': 'cleanup',
                'rename': 'rawRename',
                'exif': 'copyExiftoJpeg',
                'timeadjust': 'timeadjust',
                'gps': 'gps',
                'backup': 'backup',
                'settier': 'backup',
                'rawtherapee': 'rawtherapee',
                'blend': 'blend',
                'export': 'export'
                }

    if args.maincommand in programs:
        exit(__import__(programs[args.maincommand]).run(args))
    else:
        "Error not foudn!"
    print(programs)


if __name__ == "__main__":
    main()
