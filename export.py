#!/usr/bin/env python3

from helper.create_folder import create_folder
import os
import glob
import threading
import pprint

from helper.create_folder import create_folder


def log(msg):
    print(f'info: {msg}')


def logError(msg):
    print(f"ERR: {msg}")

def process_file(item, args):
    format = 'jpg'
    overwrite = ""
    output_switch = '-o'
    if args.keep_pp3:
        output_switch = "-O"
    if args.overwrite:
        overwrite = "-Y "
    cmd = f'rawtherapee-cli {output_switch} {args.dest}/{args.prefix}{item[2]}.{format} -q {overwrite}-p {item[0]} -c {item[1]}'
    print(cmd)
    os.system(cmd)


class Export:

    def thread(self, thread_number):
        work = self.getNext()
        while True:
            print(f"starting {work}")
            process_file(work, self.args)
            try:
                work = self.getNext()
            except StopIteration:
                print(f"{thread_number} this thread is done")
                return
        logError("Iteration didn´t stop, something went wrong")


    def getPathNames(self, name):
        folder, file = name.rsplit("/", 1)

        if self.args.source.startswith("/"):
            folder = self.args.source
        else:
            folder += "/" + self.args.source

        file = file.rsplit(".jpg", 1)[0].rsplit(".JPG", 1)[0]
        out_base_filename = file
        source_file = f"{folder}/{file.rsplit('-', 1)[0]}.CR3"
        if not (os.path.exists(source_file) and os.path.isfile(source_file)):
            logError(f"SKIPPING {name} : Source file {source_file}, not found!")
            return None, None
        return source_file, out_base_filename

    def getNext(self):
        return next(self.list_iterator)

    def getSourcesList(self):
        list = []
        for item in self.pp3list:
            source, basename = self.getPathNames(item)
            if source is not None:
                list.append((item, source, basename))
        return list

    def process_inputs(self):
        files = []
        for input in self.args.input:
            if not os.path.exists(input):
                logError(f"Input {input} not found, skipping")
                continue
            if os.path.isfile(input):
                files.append(input)
                log(f"{input} added to file list.")
                continue
            if os.path.isdir(input):
                log(f'adding .pp3 files inside {input} to files list')
                files.extend(glob.glob(f'{input}/*.pp3'))
                continue
            logError(f"Input exists but isn't a folder or file")

        return files

    def __init__(self, args):
        self.args = args
        self.pp = pprint.PrettyPrinter(indent=2)
        self.pp3list = self.process_inputs()
        self.paths = self.getSourcesList()
        self.list_iterator = iter(self.paths)
        self.thread_list = []
        create_folder(self.args.dest)

        for i in range(self.args.threads):
            t = threading.Thread(target=self.thread, args=(i,))
            self.thread_list.append(t)
            t.start()

        for t in self.thread_list:
            t.join()





        # for item in self.paths:
        #     process_file(item, self.args)
        # self.pp.pprint(self.paths)


def run(args,):
    export = Export(args)

#
# real	1m4,034s
# user	7m47,301s
# sys	0m44,779s
# real	1m4,446s
# user	7m46,994s
# sys	0m43,901s
# real	0m58,250s
# user	8m56,319s
# sys	0m42,813s
